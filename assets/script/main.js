window.addEventListener('load', () => {
  const el = document.querySelector('.mobile__button');

  el.addEventListener('click', () => {
    document.querySelector('.mobile__nav')
      .classList.toggle('show');
  }, false);

}, false);
